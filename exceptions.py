# This file is part health_snomed_ct module for GNU Health.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.

from trytond.exceptions import UserError


class ConceptAlreadyLoaded(UserError):
    pass
