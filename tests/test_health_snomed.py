import unittest
import trytond.tests.test_tryton
from trytond.tests.test_tryton import ModuleTestCase


class HealthSnomedTestCase(ModuleTestCase):
    '''
    Test Health Snomed CT module
    '''
    module = 'health_snomed_ct'


def suite():
    suite = trytond.tests.test_tryton.suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
        HealthSnomedTestCase))
    return suite
