#  -*- coding: utf-8 -*-
# This file is part health_snomed_ct module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import health_snomed_ct

from .wizard import chief_complaint_load_snomed
from .wizard import evaluation_load_snomed
from .wizard import snomed_templates_wizard


def register():
    Pool.register(
        health_snomed_ct.SnomedConfig,
        health_snomed_ct.PatientEvaluation,
        health_snomed_ct.SnomedEvaluation,
        health_snomed_ct.SnomedEvaluationChiefComplaint,
        snomed_templates_wizard.LoadSnomedTemplateStart,
        snomed_templates_wizard.LoadSnomedTemplateResult,
        snomed_templates_wizard.LoadSnomedTemplateResultLine,
        module='health_snomed_ct', type_='model')
    Pool.register(
        evaluation_load_snomed.LoadSnomed,
        chief_complaint_load_snomed.LoadSnomedChiefComplaint,
        module='health_snomed_ct', type_='wizard')
