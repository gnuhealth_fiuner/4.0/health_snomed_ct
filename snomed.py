# -*- coding: utf-8 -*-
# This file is part health_snomed_ct module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
import requests

from trytond.pool import Pool


class SnomedService(object):
    'Snomed Service'

    @classmethod
    def get_data(cls, section='concepts', params={}, limit=10,
            offset=0):
        SnomedConfig = Pool().get('gnuhealth.snomed.config')
        config = SnomedConfig(1)

        url = config.url if config.url[-1] == '/' else config.url + '/'
        url += section
        headers = {
            'Accept-Language': config.iso_lang,
            }
        if config.app_id:
            headers['app_id'] = config.app_id
        if config.app_key:
            headers['app_key'] = config.app_key
        payload = {
            'limit': str(limit),
            'offset': str(offset)
            }
        for i in params:
            payload[i] = params[i]

        try:
            response = requests.get(url, headers=headers, params=payload,
                timeout=20)
            print(response)
            if response.status_code == 200:
                return response.json()
            else:
                return None
        except Exception:
            return None
