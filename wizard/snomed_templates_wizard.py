from trytond.model import fields, ModelView
from trytond.wizard import StateView, StateTransition, Button

from ..snomed import SnomedService
from ..exceptions import ConceptAlreadyLoaded


class LoadSnomedTemplateStart(ModelView):
    'Load Snomed - Start'
    __name__ = 'gnuhealth.load_snomed.start'

    ecl = fields.Text('ECL')
    lines = fields.One2Many(
        'gnuhealth.load_snomed.result.line',
        'load_snomed_start', 'Condition', readonly=True)
    search_value = fields.Char('Search')
    page_limit = fields.Selection([
        (5, '5'),
        (10, '10'),
        (20, '20'),
        (50, '50'),
        ], 'Lines to show', sort=False,
        help='Quantity of lines to show at a time')
    offset = fields.Integer('Offset')


class LoadSnomedTemplateResult(ModelView):
    'Load Snomed Condition Result'
    __name__ = 'gnuhealth.load_snomed.result'

    lines = fields.One2Many(
        'gnuhealth.load_snomed.result.line',
        'load_snomed_result', 'Result')
    search_value = fields.Char('Results for', readonly=True)
    nav_legend = fields.Char('Found', readonly=True)
    total = fields.Integer('Total')

    @fields.depends('lines')
    def on_change_lines(self):
        concepts = [line.concept_id for line in self.lines]
        warning = self.check_concepts(concepts)
        if warning:
            raise ConceptAlreadyLoaded(
                                gettext('health_snomed_ct.msg_concept_already_loaded',
                                    {'concepts': warning}))

    @staticmethod
    def check_concepts(concepts):
        if len(concepts) > len(set(concepts)):
            return str(concepts)
        return None


class LoadSnomedTemplateResultLine(ModelView):
    'Load Snomed Condition Result Lines'
    __name__ = 'gnuhealth.load_snomed.result.line'

    load_snomed_result = fields.Many2One(
        'gnuhealth.load_snomed.result',
        'Load Snomed', required=True, ondelete='CASCADE')
    load_snomed_start = fields.Many2One(
        'gnuhealth.load_snomed.start',
        'Load Snomed')
    concept_id = fields.Char('Concept ID', required=True)
    description = fields.Char('Description', required=True)
    preferred_term = fields.Char('Preferred term')
    selected = fields.Boolean('Selected',
        help='Check to include item in the condition')


class LoadSnomedTemplate():

    start_state = 'start'
    start = StateView('gnuhealth.load_snomed.start',
        'health_snomed_ct.load_snomed_start_view_form', [
            Button('Search', 'result', 'tryton-search', default=True),
            Button('Cancel', 'end', 'tryton-cancel'),
            ])
    result = StateView('gnuhealth.load_snomed.result',
        'health_snomed_ct.load_snomed_result_view_form', [
            Button('Search again', 'start', 'tryton-search', default=True),
            Button('Next', 'result_fwd', 'tryton-forward'),
            Button('Load', 'load', 'tryton-save'),
            Button('Cancel', 'end', 'tryton-cancel'),
            ])
    result_bck = StateView('gnuhealth.load_snomed.result',
        'health_snomed_ct.load_snomed_result_view_form', [
            Button('Search again', 'start', 'tryton-search', default=True),
            Button('Go back', 'result_bck', 'tryton-back'),
            Button('Next', 'result_fwd', 'tryton-forward'),
            Button('Load', 'load', 'tryton-save'),
            Button('Cancel', 'end', 'tryton-cancel'),
            ])
    result_fwd = StateView('gnuhealth.load_snomed.result',
        'health_snomed_ct.load_snomed_result_view_form', [
            Button('Search again', 'start', 'tryton-search', default=True),
            Button('Go back', 'result_bck', 'tryton-back'),
            Button('Next', 'result_fwd', 'tryton-forward'),
            Button('Load', 'load', 'tryton-save'),
            Button('Cancel', 'end', 'tryton-cancel'),
            ])
    load = StateTransition()

    def default_start(self, fields):
        page_limit = getattr(self.start, 'page_limit', None)
        if page_limit:
            return {'page_limit': page_limit}
        else:
            return {'page_limit': 20}

    def default_result(self, fields):
        if not self.start.offset:
            self.start.offset = 0
        return self.process_result()

    def default_result_bck(self, fields):
        if self.start.offset < self.start.page_limit:
            self.start.offset = 0
        else:
            self.start.offset -= self.start.page_limit
        return self.process_result()

    def default_result_fwd(self, fields):
        self.start.offset += self.start.page_limit
        if self.start.offset > self.result.total:
            self.start.offset -= self.start.page_limit
        return self.process_result()

    def process_result(self):
        params = {
            'activeFilter': 'true',
            'term': self.start.search_value,
            'ecl': self.start.ecl
            }
        print(params)
        result = SnomedService.get_data(
            section='MAIN/concepts', params=params,
            limit=self.start.page_limit, offset=self.start.offset)
        print(result)
        data = []
        if result:
            for i in result['items']:
                data.append({
                    'concept_id': i['conceptId'],
                    'description': i['fsn'] and i['fsn']['term'] or '',
                    'preferred_term': i['pt'] and i['pt']['term'] or ''
                    })
        total = result and result['total'] or 0
        up_to = self.start.offset + self.start.page_limit \
            if (self.start.offset + self.start.page_limit) < total else total
        offset_str = str(self.start.offset + 1) + '-' + str(up_to)
        found = offset_str + ' de ' + str(total)
        return {
            'search_value': self.start.search_value,
            'nav_legend': found,
            'total': total,
            'lines': data,
            }

    def _load_lines(self):
        load_lines = []
        lines = [line for line in self.result.lines
            if line.selected is True and line.concept_id]
        bck_lines = getattr(self.result_bck, 'lines', None)
        if bck_lines:
            lines.extend([line for line in bck_lines
                if line.selected is True and
                    not any(True for elem in lines
                    if elem.concept_id == line.concept_id)])
        fwd_lines = getattr(self.result_fwd, 'lines', None)
        if fwd_lines:
            lines.extend([line for line in fwd_lines
                if line.selected is True and
                    not any(True for elem in lines
                    if elem.concept_id == line.concept_id)])
        for line in lines:
            data = {
                'concept_id': line.concept_id,
                'description': line.description,
                'preferred_term': line.preferred_term,
                }
            load_lines.append(data)
        return load_lines
