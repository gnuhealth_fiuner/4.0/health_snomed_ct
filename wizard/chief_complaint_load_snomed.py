# -*- coding: utf-8 -*-
# This file is part health_snomed_ct module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.model import ModelView, fields
from trytond.wizard import Wizard, StateView, StateTransition, Button
from trytond.pool import Pool
from trytond.transaction import Transaction

from ..snomed import SnomedService
from ..exceptions import ConceptAlreadyLoaded
from .snomed_templates_wizard import LoadSnomedTemplate

class LoadSnomedChiefComplaint(LoadSnomedTemplate, Wizard):
    'Load Snomed Chief Complaint'
    __name__ = 'gnuhealth.patient.evaluation.snomed_chief_complaint.load'

    def default_start(self, fields):
        pool = Pool()
        SnomedConfig = pool.get('gnuhealth.snomed.config')
        start = super().default_start(fields)
        ecl = SnomedConfig(1).ecl_chief_complaint
        start['ecl'] = ecl
        return start

    def transition_load(self):
        pool = Pool()
        SnomedChiefComplaint = pool.get('gnuhealth.patient.evaluation.snomed_chief_complaint')

        # cargamos las líneas
        load_lines = self._load_lines()
        if load_lines:
            for line in load_lines:
                line['evaluation'] = Transaction().context['active_id']
            SnomedChiefComplaint.create(load_lines)
        return 'end'
